# Image segmentation
This project implements neural network for semantic segmentation (find the road at aerial images) in [Tensorflow](https://github.com/tensorflow/tensorflow/) . 

# Project overview

The main file of the project is *convolutional_autoencoder.py*, which contains code for dataset processing (class Dataset), model definition (class Model) and also code for training. 

To abstract layers in the model, we created *layer.py* class interface. This class has currently two implementations: *conv2d.py* and *max_pool_2d.py*.

To infer on the trained model, have a look at *infer.py* file.

Finally, there are several folders:
- data* contain preprocessed dataset
- imgaug contains code for data augmentation 

# Model architecture

## General overview 
There are many neural network architectures for semantic image segmentation, but most of them use convolutional encoder-decoder architecture.

*Convolutional encoder-decoder architecture of popular SegNet model*

Encoder in these networks has in many cases structure similar to some image classification neural network (e.g. [vgg-16](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/slim/python/slim/nets/vgg.py)). Layers in the decoder are then ussualy inverse to layers used in the encoder (e.g. for convolution that makes its input smaller, we use deconvolution; for max_pool we use some form of "demax_pool").

## Project
Inspired by previous success of convolutional encoder-decoder architectures, we decided to implement it as well. In the encoder part, we use three similar "modules", each consisting of convolution layer with stride 2 followed by convolutution layer with stride 1 and no-overlapping max_pool with kernel 2. The decoder section then for each layer in the encoder contains its "counter-part" (network output dimension == input dimension):
- for no-shrinking convolution layer use the same layer
- for shrinking convolution layer use transposed deconvolution with same arguments
- for max pool layer use nearest neighbour upsampling (tf.image.resize_nearest_neighbor)

We also found that adding skip-links from encoder to decoder makes the model perform better (~1%).

<center>
<img src="https://raw.githubusercontent.com/arahusky/Tensorflow-Segmentation/master/model.png" width="400">
*Convolutional encoder-decoder architecture used in this project*
</center>

# Requirements:
- Python 3.5
- Tensorflow > 1.0
- Opencv 3.x
